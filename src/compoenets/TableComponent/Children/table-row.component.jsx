import React, { Component } from "react";
import DropDownList from "../../CommonComponents/dropdown-component";
// Table Row Component
class TableRow extends Component {
  // Inter mediate function to handle dropdown select change
  onselectedDropDown = (dropDown) => {
    this.props.onChangeDropDown(this.props.data, dropDown);
  };
  // Inter mediate function to handle dropdown boolean value change
  onChangedBooleanValue = (booleanValue) => {
    this.props.onChangeValueField(this.props.data, booleanValue.value);
  };
  // Inter mediate function to handle value change
  onChangedValue = (value) => {
    this.props.onChangeValueField(this.props.data, value);
  };
  // Inter mediate function to handle variable name change
  onChangedName = (value) => {
    this.props.onChangeNameField(this.props.data, value);
  };
  // Inter mediate function to get styles for input variable name field
  getNameFeildInputStyles(isEnabled, nameFeildError) {
    let styles = {};
    if (isEnabled && nameFeildError) {
      const style1 = {
        pointerEvents: "all",
        borderColor: "red",
      };
      styles = Object.assign(styles, style1);
    } else if (isEnabled && !nameFeildError) {
      const style2 = {
        pointerEvents: "all",
      };
      styles = Object.assign(styles, style2);
    } else if (!isEnabled) {
      const style3 = {
        pointerEvents: "none",
      };
      styles = Object.assign(styles, style3);
    }
    return styles;
  }
  // Inter mediate function to get styles for variable value field
  getValueFeildInputStyles(isEnabled, valueFeildError) {
    let styles = {};
    if (isEnabled && valueFeildError) {
      const style1 = {
        pointerEvents: "all",
        borderColor: "red",
      };
      styles = Object.assign(styles, style1);
    } else if (isEnabled && !valueFeildError) {
      const style2 = {
        pointerEvents: "all",
      };
      styles = Object.assign(styles, style2);
    } else if (!isEnabled) {
      const style3 = {
        pointerEvents: "none",
      };
      styles = Object.assign(styles, style3);
    }
    return styles;
  }
  render() {
    // Destructor for props
    const { data, dropDown, onEditToggle, booleanDropDown } = this.props;

    // Style Declerations
    const styleDisable = {
      pointerEvents: "none",
    };
    const styleEnalbe = {
      pointerEvents: "all",
    };
    const errorMessageStyle = {
      fontSize: "12px",
      color: "red",
      fontWeight: "bold",
    };
    // Data decerations to be used in rendeing
    const isEnabled = data["isEnabled"];
    const varient = !isEnabled ? "primary" : "success";
    const editButtonName = !isEnabled ? "Edit" : "Save";
    const selectedType = data[2];
    const valiableName = data[1];
    const value = data[3];
    const nameFeildError = data["error"]["nameField"]["value"];
    const nameFeildErrorMsg = data["error"]["nameField"]["msg"];
    const valueFeildError = data["error"]["valueField"]["value"];
    const valueFeildErrorMsg = data["error"]["valueField"]["msg"];

    let valueFieldTemplate = <input></input>;
    const styleNameInput = this.getNameFeildInputStyles(
      isEnabled,
      nameFeildError
    );
    const styleValueInput = this.getValueFeildInputStyles(
      isEnabled,
      valueFeildError
    );

    // Canditional Change in Value feild template
    if (selectedType === "Boolean") {
      valueFieldTemplate = (
        <DropDownList
          style={isEnabled ? styleEnalbe : styleDisable}
          isEnabled={isEnabled}
          dropDown={booleanDropDown}
          onChangeDropDown={this.onChangedBooleanValue}
          selectedType={value}
          variant="btn-primary-outline border border-secondary"
        ></DropDownList>
      );
    } else {
      valueFieldTemplate = (
        <React.Fragment>
          <input
            className="form-control"
            style={styleValueInput}
            type={selectedType === "Double" ? "number" : "text"}
            onBlur={(event) => this.onChangedValue(event.target.value)}
            maxLength="32"
            defaultValue={value}
          ></input>
          <span style={errorMessageStyle}>{valueFeildErrorMsg}</span>
        </React.Fragment>
      );
    }

    return (
      <tr>
        <td>
          <span>{data.id}</span>
        </td>
        <td>
          <React.Fragment>
            <input
              className="form-control"
              style={styleNameInput}
              onBlur={(event) => this.onChangedName(event.target.value)}
              maxLength="32"
              defaultValue={valiableName}
            ></input>
            <span style={errorMessageStyle}>{nameFeildErrorMsg}</span>
          </React.Fragment>
        </td>
        <td>
          <DropDownList
            style={isEnabled ? styleEnalbe : styleDisable}
            isEnabled={isEnabled}
            dropDown={dropDown}
            onChangeDropDown={this.onselectedDropDown}
            selectedType={selectedType}
            variant="btn-primary-outline border border-warning"
          ></DropDownList>
        </td>
        <td>{valueFieldTemplate}</td>
        <td>
          <button
            onClick={() => onEditToggle(data)}
            className={`btn-${varient}-outline border border-${varient} btn-sm m-2`}
          >
            {editButtonName}
          </button>
        </td>
      </tr>
    );
  }
}

export default TableRow;
